/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temamac;

/**
 *
 * @author hammi
 */
public class Processus {
    int id_processus;// les identifiants sont donnés de façon incrementale
    int priorite;//entre 1 et 5
    int harrivee;//temps d'arrive
    Activite activite;//
}

class Activite {
    Maillon_Activite premier;
    Maillon_Activite dernier;
}

class Maillon_Activite {
    Tranche tranche;
    Maillon_Activite suivant;
}

class Maillon {
    Processus processus;
    Maillon suivant;
}

class File {
    Maillon premier;
    Maillon dernier;
}

class Algo {
    int type;
    int quantum;
}

class Tranche {
    int duree;
    int type; //0 pour le calcul, 1, 2 ou 3 selon l'unite d'E/S utilisée
}

class Ressource {
    Processus processus;
    int duree;
}