package temamac;

import java.util.Scanner;

public class TemaMac {
    static File File_global; //Contient tous les processus, elle doit être remplie au début
    static File File_attente_proc; //File d'attente du processeur
    static File File_ES_1; // File d'attente de l'unité d'E/S 1
    static File File_ES_2; // File d'attente de l'unité d'E/S 2
    static File File_ES_3; // File d'attente de l'unité d'E/S 3
    static int horloge=0; //L'horloge en unité de temps
    static Ressource processeur, ES1,ES2,ES3;
    static Algo algo;
    
    public static void main(String[] args) {
         // fonction main ... 
    }
    
    public static Activite init_file(){
    Activite a = null;
    a.premier=null;
    a.dernier=null;
    return a;
    }
    
    public static boolean est_vide(Activite a){
    if(a.premier==null)
          return true;
    return false;
    }
    
    public static Activite ajouteractivite(Activite a, Tranche t){
        Maillon_Activite m =new Maillon_Activite();
        m.tranche=t;
        m.suivant=null;
        if(est_vide(a)){
             a.premier=a.dernier=m;
             return a;
        }
        (a.dernier).suivant=m;
        a.dernier=m;
        return a;
    }
        
    public static Activite retireractivite(Activite a){
        Maillon_Activite m;
        if(est_vide(a))
            return a;
        if(a.premier==a.dernier){
            a.dernier=null;
        }
        m=a.premier;
        a.premier=(a.premier).suivant;
        return a;
    }
    
    public static Activite majtranche(Activite a, int val){
        (a.premier).tranche.duree-=val;
        return a;
    }
        
    public static Tranche tete_file(Activite a){
        return (a.premier).tranche;
    }    
    
    ////////////////////
    
    public static File enfiler(File file, Processus p,int type ){
     if((type==1)||(type==2)){

         Maillon m = new Maillon();
         m.processus=p;
         m.suivant=null;
         if(est_vide(file)){
              file.premier=m;
              file.dernier=m;
              return file;
         }
         (file.dernier).suivant=m;
         file.dernier=m;
         return file;
     }
     if(type==3||type==4){
         Maillon m = new Maillon();
         m.processus=p;
         if(est_vide(file)){
              m.suivant=null;
              file.premier=file.dernier=m;
              return file;
         }
         Processus  p2=(file.dernier).processus;
         if(p.priorite <= p2.priorite){
                m.suivant=null;
                (file.dernier).suivant=m;
                file.dernier=m;
                return file;
         }
         Maillon m2 = file.premier;
         if(p.priorite > m2.processus.priorite){
                 m.suivant=m2;
                file.premier=m;
                return file;
         }
         Maillon  m3 = m2.suivant;
         while((m3!=null)&&(p.priorite <= (m3.processus).priorite)){
              m2=m3;
              m3=m3.suivant;
         }
         m2.suivant=m;
         m.suivant=m3;
         return file;
     }
     if(type==5||type==6){
         Maillon  m = new Maillon();
         m.processus=p;
         if(est_vide(file)){
              m.suivant=null;
              file.premier=file.dernier=m;
              return file;
         }
         Processus  p2=(file.dernier).processus;
         if(((tete_file(p.activite)).duree) >= ((tete_file(p2.activite)).duree)){
                m.suivant=null;
                (file.dernier).suivant=m;
                file.dernier=m;
                return file;
         }
         Maillon  m2=file.premier;
         if((tete_file(p.activite)).duree < (tete_file((m2.processus).activite)).duree){
                 m.suivant=m2;
                file.premier=m;
                return file;
         }
         Maillon  m3=m2.suivant;
         while((m3!=null)&&((tete_file(p.activite)).duree >=(tete_file((m3.processus).activite)).duree)){
              m2=m3;
              m3=m3.suivant;
         }
         m2.suivant=m;
         m.suivant=m3;
         return file;
     }
        return null;
    }  
        
    public static boolean est_vide(File file){
    if(file.premier==null)
          return true;
    return false;
    }
    ///////////////////////
    public static File defiler(File file){
          Maillon  m;
          if(est_vide(file))
              return file;
          if(file.premier==file.dernier){
               file.dernier=null;
          }
          m=file.premier;
          file.premier=(file.premier).suivant;
          return file;
}
    
    
    public static Processus tete_file(File file){
          return(file.premier).processus;
}
            
    public static void fin_activite(Processus p){
     p.activite=retireractivite(p.activite);
     if(est_vide(p.activite)){
         System.out.println("le processus +"+p.id_processus +" à completement fini son execution à " + horloge);
     }
     else{ //le processus doit réaliser une opération d'E/S
         switch((tete_file(p.activite)).type){
             case 0:File_attente_proc=enfiler(File_attente_proc,p, algo.type); System.out.println("le processus "+p.id_processus+"est en attente du processeur à %d"+horloge);break;
             case 1:File_ES_1=enfiler(File_ES_1,p, 1); System.out.println("le processus "+p.id_processus+" est en attente de l'ES "+1+" à "+horloge);break;
             case 2:File_ES_2=enfiler(File_ES_2,p, 1); System.out.println("le processus "+p.id_processus+" est en attente de l'ES "+2+" à "+horloge);break;
             case 3:File_ES_3=enfiler(File_ES_3,p, 1); System.out.println("le processus "+p.id_processus+" est en attente de l'ES "+3+" à "+horloge);break;
         }
     }
}
            
    public static void lire_arrivees(){
     int i=0;
     String lecture="o";
     Scanner sc = new Scanner(System.in);
     
     while(lecture=="o"){
        Processus p=new Processus();
        p.id_processus=i;
        System.out.println("Donnez le temps d'arrivee du processus "+i+" :");
        p.harrivee = sc.nextInt();
        System.out.println("Donnez la priorite du processus "+i+" :");
        p.priorite = sc.nextInt();
        int tr=1;
        Activite activite;
        activite=init_file();
        int t=0;
        while(true){
            Tranche tranche = null;
              if(tr%2 > 0){
                    int duree;
                    System.out.println("Donnez la duree de calcul du processus "+i+"  pour la tranche "+tr+":");
                    duree = sc.nextInt();
                    if(duree > 0){
                        tranche.duree = duree;
                        tranche.type=0;
                        activite=ajouteractivite(activite, tranche);
                    }
                    else
                        break;
              }
              else{
                    int duree;
                    int type;
                    System.out.println("Donnez la duree d'E/S du processus "+i+"  pour la tranche "+tr+":");
                    duree = sc.nextInt();
                    if(duree == 0){
                        break;
                    }
                    System.out.println("Donnez le numéro d'E/S demande par le processus "+i+" pour la tranche "+tr+":");
                    type = sc.nextInt();
                    tranche.duree=duree;
                    tranche.type=type;
                    activite=ajouteractivite(activite, tranche);
              }
              tr++;

        }

        p.activite=activite;
        File_global=enfiler(File_global, p, 1);
        i++;
        System.out.println("Voulez-vous entrer un autre processus (o/n)):");
        lecture = sc.next();
        
     }



}
    
    public static void choix_algorithme(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Veuillez faire un choix dans le menu suivant");
        System.out.println("1) premier arrivée premier servi");
        System.out.println("2) Round_robbin");
        System.out.println("3) Priorité");
        System.out.println("4) Priorité préemptif");
        System.out.println("5) Plus court d'abord");
        System.out.println("6) Temps restant le plus court");
        System.out.println("Votre choix :");
        algo.type = sc.nextInt();
        if(algo.type==2){
             System.out.println("Avec quel quantum :");
             algo.quantum = sc.nextInt();
        }
    }
    
    public static void majfileproc(){
        Processus P = null;
        while(!est_vide(File_global)){
            P=tete_file(File_global);
            if(P.harrivee==horloge)
                    File_attente_proc=enfiler(File_attente_proc, P ,algo.type);
        }
    } 
    
    public static File traiterES(Ressource es, File file){ //je me préoccupe par l'E/s 
        return null;
    }
    
    public static void algorithme_ordonnancement(){ 
        // il se préoccupe  du procecceur esk j'eneve le processus pour maitre un autre la duréé   COMMENT ON GERE UN PROC2SSEUR il est exécuté a chaque unité deps 
    }
    
    public static void majtempsressource(){ //ajoute une unité de temps pour les ressources occupées
	if (!ressource_libre(processeur)){
				processeur.duree=(processeur.duree)+1;
	}
	
	if (!ressource_libre(ES1)){
				ES1.duree=(ES1.duree)+1;
	}

	if (!ressource_libre(ES2)){
				ES2.duree=(ES2.duree)+1;
	}

	if (!ressource_libre(ES3)){
				ES3.duree=(ES3.duree)+1;
	}


    }
    
    public static boolean ressource_libre(Ressource r){
        if(r.processus==null)
            return true;
        return false;
    }
    
    public static Ressource init_ressource(){
        Ressource r = null;
        r.processus=null;
        r.duree=0;
        return r;
}
}
