/**
 *
 * @author hammi
 */
package javaapplication2;
import java.util.*;
import static javaapplication2.car.chercherVoiture;
import static javaapplication2.car.supprimerVoiture;

public class TestClass {
    public static void main (String []args){
        ArrayList<car> Liste = new ArrayList<>();
        
        System.out.println("AGENCE VIDE, Combien de voiture voullez vous ajouter ?");
        Scanner sc = new Scanner(System.in);
        int x = sc.nextInt();
        
        for (int i = 0; i<x ; i++){
            Liste.add(i,new car());
        }
        
        for (int i = 0; i<x ; i++){
            System.out.println("Voiture "+(i+1));
            Liste.get(i).ajouterVoiture();
            System.out.println("------------------------------");
        }
        
        System.out.println("le nb des voiture de marque MARQ est "+Liste.get(0).compteurMARQ(Liste));
        System.out.println("------------------------------");
        
        
        System.out.println("donner un num de immatriculation pour trouver une voiture");
            int num = sc.nextInt();
            if (chercherVoiture(Liste,num) != Liste.size()+1)
                System.out.println(Liste.get(chercherVoiture(Liste,num)).afficherVoiture());
            
        System.out.println("donner un num de immatriculation pour supprimer une voiture");
            num = sc.nextInt();
            if (chercherVoiture(Liste,num) != Liste.size()+1)
                supprimerVoiture(Liste,Liste.get(chercherVoiture(Liste,num)));      
    }
    
}
